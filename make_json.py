import json

def read_data():
    file = open('traffic-counts-for-select-bridges-beginning-1933-1.csv','r')
    data = file.readlines()

    # get the headers names
    keys = data[0].strip()
    keys = keys.split(',')

    # split the rest of the data
    values = data[1:]
    for ids in range(len(values)):
        values[ids] = values[ids].replace(',', ' ')
        values[ids] = values[ids].split()

        
    # make a list of the data dictionaries
    data = []
    for num_ids in range(len(values)):
        data.append({})
        for num_keys in range(len(keys)):
            if (keys[num_keys] == 'Deviation %'):
                data[num_ids][keys[num_keys]] = float(values[num_ids][num_keys])
            else:
                data[num_ids][keys[num_keys]] = int(values[num_ids][num_keys])
                
    file.close()
    return data

def makeJSONfile(path, filename,data):
    filePathName = path+filename+'.json'
    with open(filePathName, 'w') as fp:
        json.dump(data,fp)
        
def main():
    path = './'
    filename = 'bridge_data'
    data = read_data()

    makeJSONfile(path,filename,data)

main()
