import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import api

#these are the commands called by worker.py
#each takes a string that was stored under parameters in the new job dictionary created by the api and splits it into the individual parameters

#add up total bridge traffic from start year up to but not including end year
def total_years(bounds):

    #get start year, s, and end year, e
    s = int(bounds[:4])
    e = int(bounds[4:])

    #using api, retrieve an array of totals between years from redis and sum them
    data = api.retrieve_data(startYear=s, endYear=e, bridge="Total")
    total = np.nansum(data)
    
    #format results
    message = 'Total, ' + str(s) + '-' + str(e-1) + ': ' + str(total)

    return message



#get total traffic for a given bridge
def total_bridge(bounds):
    
    b = bounds
    
    #using api, rettrieve array of bridge traffic from redis and sum the values
    data = api.retrieve_data(bridge=b)
    total = np.nansum(data)

    #format output
    message = 'Total, ' + b + ': ' + str(total)

    return message



#calculate average bridge traffic from start year to but not including end year
def mean_years(bounds):

    #get start year and end year
    s = int(bounds[:4])
    e = int(bounds[4:])
    
    #get bridge data from start year up to but not including end year
    data = api.retrieve_data(startYear=s, endYear=e)

    #extract all bridge traffic counts from dictionary and put them in arr
    keys = ['Rip Van Winkle Bridge', 'Kingston-Rhinecliff Bridge', 'Mid-Hudson Bridge', 'Newburgh-Beacon Bridge','Bear Mountain Bridge']
    arr = []
    for i in data:
        for k in keys:
            #encode the keys used to locate value in dictionary, decode the values retrieved and cast them as floats
            #j = k.encode('utf-8')
            #m = float(i[j].decode('utf-8'))

            #cast values for each bridge as floats and append them to an array
            m = float(i[k])
            arr.append(m)

    #calculate the mean
    mean = np.nanmean(arr)
    
    #format the output
    message = 'Mean, ' + str(s) + '-' + str(e-1) + ': ' + str(mean)

    return message




#calculate the mean traffic for a bridge from start year up to but not including end year
def mean_years_bridge(bounds):

    #get start year end year and bridge
    s = int(bounds[:4])
    e = int(bounds[4:8])
    b = bounds[8:]
    
    #using api, retrieve array of bridge traffic data from redis, then calculate the mean
    data = api.retrieve_data(startYear=s, endYear=e, bridge=b)
    mean = np.nanmean(data)
    
    #format output
    message = 'Mean, ' + str(s) + '-' + str(e-1) + ',' + str(b) + ': ' + str(mean)

    return message



#create a firgure with 5 subplots, each a graph for a bridge from start year up to but not including end year
def graph_for_years(bounds):
    
    #get start year and end year
    s = int(bounds[:4])
    e = int(bounds[4:])

    #using api, retrieve data between start year and end year
    data = api.retrieve_data(startYear=s, endYear=e)

    #cast values in dictionary as floats
    for i in data:
        k = i.keys()
        for j in k:
            #i[j]= float(i[j].decode('utf-8'))
            i[j] = float(i[j])

    #create pandas data frame with the year as indices
    bridges = pd.DataFrame(data)
    #bridges = bridges.set_index("Year".encode('utf-8'))
    bridges = bridges.set_index("Year")

    #identify keys of columns to be plotted
    #encode the keys because the dictionary removed from redis has bytes keys
    #cols_plot = ['Rip Van Winkle Bridge'.encode('utf-8'), 'Kingston-Rhinecliff Bridge'.encode('utf-8'), 'Mid-Hudson Bridge'.encode('utf-8'), 'Newburgh-Beacon Bridge'.encode('utf-8'),'Bear Mountain Bridge'.encode('utf-8')]
    cols_plot = ['Rip Van Winkle Bridge', 'Kingston-Rhinecliff Bridge', 'Mid-Hudson Bridge', 'Newburgh-Beacon Bridge','Bear Mountain Bridge']

    #specify subplots
    axes = bridges[cols_plot].plot(marker='*', alpha=0.5, linestyle='-', figsize=(15, 15), subplots=True)
    for ax in axes:
        ax.set_ylabel('Annual Bridge Traffic')
    
    #format file name
    name = 'source/images/' + 'bridge_traffic_' + str(s) + '_' + str(e) + '.png'

    #save file in images folder (check folder for a few of our results)
    plt.savefig(name)
    
    #retrieve bytes
    file_bytes = open(name, 'rb').read()

    #return bytes 
    return file_bytes



#create pie chart comparing the traffic on each bridge for a given year    
def pie_year(bounds):

    #get year
    s = int(bounds)

    #using api, get data for the given year, extract the dictionary from the arrray
    data = api.retrieve_data(startYear=s, endYear=(1+s))
    data = data[0]

    #cast values in dictionary as floats and save them in an array 
    b = ['Rip Van Winkle Bridge', 'Kingston-Rhinecliff Bridge', 'Mid-Hudson Bridge', 'Newburgh-Beacon Bridge','Bear Mountain Bridge']
    a = []

    for i in b:
        #c = data[i.encode('utf-8')]
        #a.append(float(c.decode('utf-8')))
        c = data[i]
        a.append(float(c))
    
    #name plot
    key = "Traffic: " + str(s)

    #create pandas data frame
    df = pd.DataFrame({key: a}, index=b)

    #create pie chart 
    plot = df.plot.pie(y=key, figsize=(13, 13))

    #name figure and save it under source/images (look in folder for a few of our results)
    name = 'source/images/' + "Traffic_"+str(s)+".png"
    plt.savefig(name)
    
    #retrieve and return bytes
    file_bytes = open(name, 'rb').read()

    return file_bytes




#calculate the rolling mean for a given window of years (for example the rolling mean over ten years) for a given bridge
#plot next to annua data for the same bridge
def rolling_mean(bounds):

    #retrieve rolling mean window and bridge name
    y = int(bounds[:2])
    b = bounds[2:]

    #using api, get data
    data = api.retrieve_data()

    #cast values in data as floats
    for i in data:
        k = i.keys()
        for j in k:
            #i[j]= float(i[j].decode('utf-8'))
            i[j] = float(i[j])

    #create pandas data frame, set index as year
    bridges = pd.DataFrame(data)
    #bridges = bridges.set_index('Year'.encode('utf-8'))
    bridges = bridges.set_index('Year')

    #calculate rolling mean centered on given window of time
    #rm = bridges[b.encode('utf-8')].rolling(y, center=True).mean()
    rm = bridges[b].rolling(y, center=True).mean()

    #name the figure
    l = str(y) + ' Year Rolling Mean'

    #plot rolling mean data and annual data, set up legend identifying each graph
    #line_up, = plt.plot(bridges[b.encode("utf-8")], label='Annual Data')
    line_up, = plt.plot(bridges[b], label='Annual Data')
    line_down, = plt.plot(rm, label=l)
    plt.legend([line_up, line_down], ['Annual Data', l])

    #label axes and title graph
    plt.xlabel('Year')
    plt.ylabel('Traffic')
    plt.title(b)
    
    #name figure and save it in images/source (check the directory for a few of our results)
    name = 'source/images/' + b[:3] + '_rolling_' + str(y) + '.png'
    
    plt.savefig(name)
    
    #retrieve file bytes and return them
    file_bytes = open(name, 'rb').read()

    return file_bytes
    
