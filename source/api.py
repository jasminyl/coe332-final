from flask import Flask, jsonify, request, send_file
import io
import os
import json
from hotqueue import HotQueue
import redis
import uuid
import time

api = Flask(__name__)

#get redis ip address and port
REDIS_IP = os.environ.get('REDIS_IP')
REDIS_PORT = os.environ.get('REDIS_PORT')

r = redis.StrictRedis(host=REDIS_IP, port=6379, db=0)

#queue
_q = HotQueue("queue", host=REDIS_IP, port=6379, db=1)

#jobs log
_j = redis.StrictRedis(host=REDIS_IP, port=6379, db=2)


#input bridge data to redis
data = json.load(open('bridge_data.json', 'r'))
length = len(data)
hashKey = []

for i in range(length):
    hashName = str(data[i]["Year"])
    year_data = {"Year": data[i]["Year"], "Rip Van Winkle Bridge": data[i]["Rip Van Winkle Bridge"], "Kingston-Rhinecliff Bridge": data[i]["Kingston-Rhinecliff Bridge"], "Mid-Hudson Bridge": data[i]["Mid-Hudson Bridge"], "Newburgh-Beacon Bridge": data[i]["Newburgh-Beacon Bridge"], "Bear Mountain Bridge": data[i]["Bear Mountain Bridge"], "Total": data[i]["Total"], "Deviation": data[i]["Deviation"], "Deviation %": data[i]["Deviation %"]}
    r.hmset(hashName, year_data)
    



# functions for job info



#generates sting UUID for job ID and record ID (used each time a job's status is updated)
def get_ID():
    return str(uuid.uuid4())

#returns dictionary value to store in redis while job waits on th queue
def new_job_dict(jid, command, parameters, jtype):
    t = time.time()
    status = 'Job Waiting'
    
    if type(jid) == str:
        return {'id': jid,
                'status': status,
                'time': t,
                'command': command,
                'parameters': parameters,
                'type': jtype
                }
    return {'id': jid.decode('utf-8'),
            'status': status.decode('utf-8'),
            'time': t.decode('utf-8'),
            'command': command.decode('utf-8'),
            'parameters': parameters.decode('utf-8'),
            'type': jtype.decode('utf-8')
            }

#returns dictionary value to input into redis when a job is picked up by a worker off the queue    
def in_progress_job_dict(jid):
    t = time.time()
    status = 'In Progress'

    if type(jid) == str:
        return {'id': jid,
                'status': status,
                'time': t
                }
    return {'id': jid.decode('utf-8'),
            'status': status.decode('utf-8'),
            'time': time.decode('utf-8')
            }

#returns dictionary value to input into redis when the worker completes a job
def complete_job_dict(jid, output, jtype):
    t = time.time()
    status = 'Completed'

    if type(jid) == str:
        return {'id': jid,
                'status': status,
                'time': t,
                'result': output,
                'type': jtype
                }
    return {'id': jid.decode('utf-8'),
            'status': status.decode('utf-8'),
            'time': time.decode('utf-8'),
            'result': output.decode('utf-8'),
            'type': jtype
            }


#puts a new jobs ID on the queue, to be picked up by a worker
def enqueue(jid):
    _q.put(jid)


#takes a job dictionary, retrieves a record ID, stores in redis
def log_job(job_dict):
    key = get_ID()
    value = job_dict

    _j.hmset(key, value)

#process to follow when a 'get' input is called
def manage_job(command, parameters, jtype):
    jid = get_ID()
    d = new_job_dict(jid, command, parameters, jtype)
    log_job(d)
    enqueue(jid)

    #return a json with directions for retrieving results depending on job type
    return jsonify('Received! ' + jid + ' is your job\'s ID. To check your job\'s status and receive your results use \'http://0.0.0.0:5000/Status/<ID>\' for a computational job or \'http://0.0.0.0:5000/Status/figure/<ID>\' for a plotting job. Note that for a plotting job, if you are using curl, you will have to add \' -O\' (a space dash capital O) after the url.')

#(for a computational job) searches redis for most recent record of job, if the status is "Completed", return result
def retrieve_status(jid):
    #grab keys in jobs redis
    k = _j.keys('*') 
    #set inital value of time to 0, so that the first dictionary found will definitely have a greater time stamp
    time = 0
    status = ''

    #iterate through redis keys
    for i in k:
        #decode the each key, and retrieve the job ID at that key
        key = i.decode('utf-8')
        _id = _j.hget(key, 'id')

        #check whether job ID matches given job ID
        if(_id.decode('utf-8') == jid):
            
            #if the job IDs match, get time and check whether it is larger than any preivious saved time
            _t = _j.hget(key, 'time')
            
            if(float(_t.decode('utf-8'))>time):
                #if the time is greater, save it, and retrieve and save the status
                time = float(_t.decode('utf-8'))
                
                _s = _j.hget(key, 'status')
                status = _s.decode('utf-8')
                
                #check whether status is completed
                if(status == 'Completed'):
                    
                    #get the job type and confirm that it is computational
                    _tp = _j.hget(key, 'type')

                    if(_tp.decode('utf-8') == 'comp'):

                        #if the job is computational, retrieve, decode, and return the result
                        _r = _j.hget(key, 'result')
                        result = _r.decode('utf-8')

                        message = {'Status': 'Completed', 'Result': result}
                        return jsonify(message)

                    #redirect the user if this is a plotting job
                    else:

                        message = 'This is not a computational job. Try \'http://0.0.0.0/Status/figure/<ID>\'. Note that for a plotting job, if you are using curl, you will have to add \' O\' (a space dash capital O) after the url.'
                        return jsonify(message)
    
    #return status if found and an error if not found
    if(status == ''):
        message = 'Job not found. Please check your job ID.'
        return jsonify(message)
    
    message = {'Status': status}
    return jsonify(message)



#searches redis for most recent dict matching jid for a graphing job, returns status if incomplete, returns graph if complete
def retrieve_graph(jid):
    #retrieve all keys in _j redis
    k = _j.keys('*')
    #save an initial time stamp as 0, so that the first time found will be greater
    time = 0
    status = ''

    #iterate through keys, decoding each one
    for i in k:
        key = i.decode('utf-8')

        #get the job ID at the given key, and check whether it matches the given job ID
        _id = _j.hget(key, 'id')

        if(_id.decode('utf-8') == jid):

            #get the time and check whether it is greater than the saved time
            _t = _j.hget(key, 'time')

            if(float(_t.decode('utf-8'))>time):
                #if the time is greater than the saved time, save it, then retrieve and save the status
                time = float(_t.decode('utf-8'))

                _s = _j.hget(key, 'status')
                status = _s.decode('utf-8')

                #check whether the status is completed
                if(status == 'Completed'):

                    #if the status is completed, get the type and confirm that this is a plotting job
                    _tp = _j.hget(key, 'type')

                    if(_tp.decode('utf-8') == 'plot'):
                        
                        #return the image bytes
                        plot = _j.hmget(key, 'result')
                        name = jid + '.png'
                        return send_file(io.BytesIO(plot[0]), mimetype='image/png', as_attachment=True, attachment_filename=name)
                    
                    #redirect the user if the job ID is not a plotting job
                    else:
                        
                        message = 'This is not a plotting job. Try \'http://0.0.0.0/Status/<ID>\''
                        return jsonify(message)
    
    #return status or an error if the job ID is not found
    if(status == ''):
        message = 'Job not found. Please check your job ID.'
        return jsonify(message) 
    
    message = {'Status': status}
    return jsonify(message)

#used by worker to retrieve job dictionary from redis
#note that the contents of the returnd dictionary still have to be decoded from unicode
def retrieve_job_data(jid):
    #get all keys in _j redis
    k = _j.keys('*')

    #iterate through keys, decoding each one
    for i in k:
        key = i.decode('utf-8')

        #get the Job ID under each key, and check whether it matches the given job ID
        _id = _j.hget(key, 'id')

        if(_id.decode('utf-8') == jid):
            
            #get the status, and confirm that the job ID is from a dictionary with 'Job Waiting' status
            _s = _j.hget(key, 'status')
            status = _s.decode('utf-8')

            #return the job data if found, otherwise return an error message
            if(status == 'Job Waiting'):
                info = _j.hgetall(key)
                return info

    return 'Error'


#retrieves specified bridge data from redis
def retrieve_data(startYear = 1934, endYear = 2017, bridge = None):
    bridge_start_end = []
    
    #iterate through years, the redis keys for the r database
    for i in range(startYear,endYear):

        #retrieve the dictionary matching each year and decode its keys and values
        hashKey = str(i)
        d = r.hgetall(hashKey)
        data = {k.decode('utf-8'):v.decode('utf-8') for k,v in d.items() }
        
        #if there is a bridge parameter, extract the bridge's traffic data and cast it as a float
        if (bridge != None):
            data = data[bridge]
            data = float(data)

        #append the data to an array and return it
        bridge_start_end.append(data)
    
    return bridge_start_end

#test endpoint
@api.route('/')
def test():
    return 'See README.md for Endpoints\n'

#endpoint for users to check the status of their computational job by jid. Will return results if the job is complete
@api.route('/Status/<string:jid>', methods=['GET'])
def check_status(jid):
    
    if(type(jid) == str):

        message = retrieve_status(jid)
        return message

    return '', 400

#endpoint for users to check the status of their plotting job by jid. Will return binary file if the job is complete
@api.route('/Status/figure/<string:jid>', methods=['GET'])
def get_figure(jid):

    if(type(jid) == str):

        message = retrieve_graph(jid)
        return message

    return '', 400

#dictionary to convert user inputs to data keys
decoder = {'RVW': 'Rip Van Winkle Bridge', 'KR': 'Kingston-Rhinecliff Bridge', 'MH': 'Mid-Hudson Bridge', 'NB': 'Newburgh-Beacon Bridge', 'BM': 'Bear Mountain Bridge'}

#data retrieval endpoints
@api.route('/data', methods=['GET'])
def get_data():

    output = retrieve_data()
    return jsonify(output)

@api.route('/<int:s_y>/<int:e_y>', methods=['GET'])
def get_data_by_years(s_y, e_y):
    
    if(type(s_y) == int and type(e_y) == int):
        
        output = retrieve_data(startYear = s_y, endYear = e_y)
        return jsonify(output)

    return '', 400

@api.route('/<string:b>', methods=['GET'])
def get_data_by_bridge(b):

    if(type(b) == str):

        if(b in decoder.keys()):
                        
            br = decoder[b]
        
            output = retrieve_data(bridge = br)
            return jsonify(output)

        return '', 404

    return '', 400

#computational job endpoints
@api.route('/total/<int:s_y>/<int:e_y>', methods=['GET'])
def get_total_between_years(s_y, e_y):
    
    if(type(s_y) == int and type(e_y) == int):
    
        par = str(s_y) + str(e_y)
        message = manage_job(0, par, 'comp')
        return message

    return '', 400

@api.route('/total/<string:b>', methods=['GET'])
def get_total_by_bridge(b):
    
    if(type(b) == str):

        if(b in decoder.keys()):
            br = decoder[b]
    
            message = manage_job(1, br, 'comp')
            return message
        
        return '', 404

    return '', 400

@api.route('/mean/<int:s_y>/<int:e_y>', methods=['GET'])
def get_mean_between_years(s_y, e_y):
    
    if(type(s_y) == int and type(e_y) == int):

        par = str(s_y) + str(e_y)
        message = manage_job(2, par, 'comp')
        return message

    return '', 400

@api.route('/mean/<int:s_y>/<int:e_y>/<string:b>', methods=['GET'])
def get_mean_by_years_and_bridge(s_y, e_y, b):
    
    if(type(s_y) == int and type(e_y) == int and type(b) == str):
    
        if(b in decoder.keys()):
            br = decoder[b]
            
            par = str(s_y) + str(e_y) + br
            message = manage_job(3, par, 'comp')
            return message

        return '', 404

    return '', 400


#plotting job endpoints
@api.route('/graph/<int:s_y>/<int:e_y>', methods=['GET'])
def graph_for_years(s_y, e_y):
    
    if(type(s_y) == int and type(e_y) == int):
    
        par = str(s_y) + str(e_y)
        message = manage_job(4, par, 'plot')
        return message

    return '', 400

@api.route('/pie/<int:s_y>', methods=['GET'])
def graph_pie_year(s_y):

    if(type(s_y) == int):

        par = str(s_y)
        message = manage_job(5, par, 'plot')
        return message

    return '', 400

@api.route('/roll/<string:b>/<int:m_y>', methods=['GET'])
def graph_roll_bridge(b, m_y):

    if(type(m_y) == int and type(b) == str):

        if(b in decoder.keys()):
                        
            br = decoder[b]
            par = str(m_y) + br
            if(m_y < 10):
                par = '0'+ par
            message = manage_job(6,  par, 'plot')
            
            return message
        
        return '', 404

    return '', 400


#posting endpoint
@api.route('/add/<int:y>', methods=['POST'])
def add_year(y):

    if(type(y) == int):

        data = request.json
        data["year"] = y

        r.hmset(y, data)

        return jsonify('Success! We have recieved your data for ' + str(y) + ' .')
    
    return '', 400

