# COE 332 Final Project



## Contributors  
Jasmin Lim, Shealyn Greer, and Katharine Fisher  



## Project Summary  
This time series data set looks at the traffic count for select bridges in New York City from 1934 up to 2017. The bridges included are: Rip Van Winkle Bridge, Kingston-Rhinecliff Bridge, Mid-Hudson Bridge, Newburgh-Beacon Bridge, and Bear Mountain Bridge. This traffic data proves useful for transportation engineers who must analyze traffic flow in order to understand how quickly the bridges are deteriorating and how often they need to undergo maintenance. Additionally, city planners can use the bridge traffic data to analyze the increase or decrease of the population in certain areas. This specific data can be further analyzed to determine whether new roadways need to be opened.  


## Endpoints and Outcomes  

### Data Retrieval Endpoints  

+ GET /data  

		returns traffic data for all five bridges from 1934 to 2017 as well as the total bridge traffic, deviation, and percent
		deviation for each year    



+ GET /<start-year>/<end-year>   

		returns all yearly data for all five bridges beginning at the start year up to but not including the end year. Years 
		should be from 1934 through 2017, unless you have posted data for a new year  



+ GET /<bridge>  

		returns all data for a particular bridge from 1934 to 2017, unless you have posted data for a new year. Use abbreviations: 
		RVW - Rip Van Winkle Bridge, KR - Kingston-Rhinecliff Bridge, MH - Mid-Hudson Bridge, NB - Newburgh-Beacon Bridge, 
		BM - Bear Mountain Bridge  



### Computational Job Endpoints  

+ GET /total/<start-year>/<end-year>  

		returns the total bridge traffic from the start year up to and not including the end year. Years should be from 1934
		through 2017, unless you have posted data for a new year  



+ GET /total/<bridge>  

		returns the total traffic for a bridge. Use abbreviations: RVW - Rip Van Winkle Bridge, KR - Kingston-Rhinecliff Bridge, 
		MH - Mid-Hudson Bridge, NB - Newburgh-Beacon Bridge, BM - Bear Mountain Bridge  



+ GET /mean/<start-year>/<end-year>   

		returns the mean traffic experienced by a bridge from the start year up to and not including the end year. Years should be from 1934
		through 2017, unless you have posted data for a new year   



+ GET /mean/<start-year>/<end-year>/<bridge>  

		returns the mean traffic on a bridge from the start year up to and not including the end year. Years should be from 1934
		through 2017, unless you have posted data for a new year. Use abbreviations: RVW - Rip Van Winkle Bridge, KR - Kingston-Rhinecliff Bridge, 
		MH - Mid-Hudson Bridge, NB - Newburgh-Beacon Bridge, BM - Bear Mountain Bridge  



### Plotting Job Endpoints  

+ GET /graph/<start-year>/<end-year>  

		returns a bytes stream image with five subplots, one for each bridge, showing traffic from the start year up to but not including the end year
		and saves a .png image to source/images. Years should be from 1934 through 2017, unless you have posted data for a new year  



+ GET /pie/<year>  

		returns a bytes stream image with a pie chart comparing traffic on each bridge for the given year and saves a .png image to source/images. Years 
		should be from 1934 through 2017, unless you have posted data for a new year  



+ GET /roll/<bridge>/<rolling-window>  

		returns a bytes stream image with a graph comparing annual traffic on a given bridge to the rolling mean of traffic over a given number of years
		and saves a .png image to source/images. Use abbreviations: RVW - Rip Van Winkle Bridge, KR - Kingston-Rhinecliff Bridge, 
		MH - Mid-Hudson Bridge, NB - Newburgh-Beacon Bridge, BM - Bear Mountain Bridge. The rolling window should be positive and less than 80  



### Retrieving Job Status and Results  

+ GET /Status/<ID>  

		returns the status of a computational job. If the job is completed, the result will be returned as well. You will recieve your job ID in 
		response to your initial job request.  



+ GET /Status/figure/<ID>   

		returns the status of a plotting job. If the job is completed, the plot will be returned as a bytes stream and saved as a .png file in 
		source/images. You will recieve your job ID in response to your initial job request. Note that if you are using curl, you will have 
		to add " -O" (a space dash capital O) after the url.   



### Posting New Data  

+ POST /add/<year>  

		allows reader to add data on traffic on the five bridges for a new year  



## Example Calls  



+ With Curl:  



```python
		curl http://0.0.0.0:5000/1948/1950
		


		Final Output:
		
		[
		
			{"Year": "1948"
			
			"Rip Van Winkle Bridge": "957702",
			
			"Kingston-Rhinecliff Bridge": "0",
			
			"Mid-Hudson Bridge": "2454479",
			
			"Newburgh-Beacon Bridge": "0",
			
			"Bear Mountain Bridge": "1044787",
			
			"Total": "4456968",
			
			"Deviation": "384851",
			
			"Deviation %": "9.45"},
		
			
			{"Year": "1949"
		
			"Rip Van Winkle Bridge": "1129075",
			
			"Kingston-Rhinecliff Bridge": "0",
			
			"Mid-Hudson Bridge": "2722172",
			
			"Newburgh-Beacon Bridge": "0",
			
			"Bear Mountain Bridge": "1179561",
			
			"Total": "5030808",
			
			"Deviation": "573840",
			
			"Deviation %": "12.88"}
			
		]
```  
			


```python
		curl http://0.0.0.0:5000/total/KR
		
		
		
		Output:
		
		"Received! <ID> is your job's ID. To check your job's status and receive your results use 'http://0.0.0.0:5000/Status/<ID>' 
		for a computational job or 'http://0.0.0.0:5000/Status/figure/<ID>' for a plotting job. Note that for a plotting job, if you 
		are using curl, you will have to add ' -O' (a space dash capital O) after the url."



		curl http://0.0.0.0:5000/Status/<ID>



		Final Output:
		
		{"Kingston-Rhinecliff Bridge, Total": 287332154}
```  



```python
		curl http://0.0.0.0:5000/add/2018 -d '{"Rip Van Winkle Bridge": 5942352,"Kingston-Rhinecliff Bridge": 8198068,
		"Mid-Hudson Bridge": 14494120,"Newburgh-Beacon Bridge": 26718598,"Bear Mountain Bridge": 7848404,"Total": 63201542,
		"Deviation": 366708,"Deviation %": 0.58}' -H 'Content-Type: application/json'
		
		
		
		Final Output:
		
		Success! Data posted for the year 2017.
```  



```python
		curl http://0.0.0.0:5000/graph/1937/1974
		
		
		
		Output:
		
		"Received! <ID> is your job's ID. To check your job's status and receive your results use 'http://0.0.0.0:5000/Status/<ID>' 
		for a computational job or 'http://0.0.0.0:5000/Status/figure/<ID>' for a plotting job. Note that for a plotting job, if you 
		are using curl, you will have to add ' -O' (a space dash capital O) after the url."



		curl http://0.0.0.0:5000/Status/figure/<ID> -O



		Final Output:
		
		An image will be returned to you in raw bytes, and a .png file will be saved in source/images.
```  



+ With Python:  



```python
		import requests

		response = requests.get("http://0.0.0.0:5000/mean/1981/1997")
		if response.status_code == 200:
			print(response.text)
		
		
		
		Output:
		
		"Received! <ID> is your job's ID. To check your job's status and receive your results use 'http://0.0.0.0:5000/Status/<ID>' 
		for a computational job or 'http://0.0.0.0:5000/Status/figure/<ID>' for a plotting job. Note that for a plotting job, if you 
		are using curl, you will have to add ' -O' (a space dash capital O) after the url."



		response = requests.get("http://0.0.0.0:5000/Status/<ID>")
		if response.status_code == 200:
			print(response.text) 




		Final Output:
		
		{"Mean, 1981-1997": 8350085.718}
```  



```python
		data = {"Rip Van Winkle Bridge": 5942352,
			
				"Kingston-Rhinecliff Bridge": 8198068,
			
				"Mid-Hudson Bridge": 14494120,
			
				"Newburgh-Beacon Bridge": 26718598,
			
				"Bear Mountain Bridge": 7848404,
			
				"Total": 63201542,
			
				"Deviation": 366708,
			
				"Deviation %": 0.58}
			
		response = requests.post("http://0.0.0.0:5000/add/2018", json = data)
		if response.status_code == 200:
			print(response.text)



		Final Output:
		
		Success! Data posted for the year 2017.
```  



```python
		import requests
		
		response = requests.get("http://0.0.0.0:5000/Status/figure/<ID>")
		if response.status_code == 200:
			print(response.text)
			
		
		
		Output:
		
		"Received! <ID> is your job's ID. To check your job's status and receive your results use 'http://0.0.0.0:5000/Status/<ID>' 
		for a computational job or 'http://0.0.0.0:5000/Status/figure/<ID>' for a plotting job. Note that for a plotting job, if you 
		are using curl, you will have to add ' -O' (a space dash capital O) after the url."
		
		
		
		response = requests.get("http://0.0.0.0:5000/Status/figure/<ID>", stream=True)

		if response.status_code == 200:
			with open('path/my_image.png', 'wb') as f:
        		for i in response:
            		f.write(i)



		Final Output: 
		
		A .png file will be saved in the path you specify as well as in source/images.
```  



## Pricing  
The method for charging for access to the API could potentially come from 1 of 3 different categories: pay-as-you-go, fixed quota, or overage model. For the purpose of this specific time series data set handling bridge traffic data, it would be best to charge the client based on the overage model. The overage model allows the client a fixed number of calls each month and charges a small overage fee if the client exceeds that specified number of calls. This model allows for predictable pricing and revenue and ensure the app is never shut down. There is a downside to this method as miscommunication around overages could occur. Within this overage model plan, there could be different price levels depending upon who the client is.  
