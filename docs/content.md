## COE 332 - Final Project Content Descriptions  

Descriptions for Final Project Content  

### Data Files  
* `traffic-counts-for-select-bridges-beginning-1933-1.csv` - Original Data file sourced from [*Data World*](https://data.world/data-ny-gov/5qpa-id23)  
* `make_json.py` - python file that transfers data from the `.csv` file into `bridge_data.json`  
* `bridge_data.json` - JSON file of data   
* `redisdb.py` - put the contents of the JSON file into a redis database  

### source/  
* `api.py` - contains API endpoint routes and function for data retrieval  
* `jobs.py` - contains functions for computational job endpoints, functions return a message with the job ID  
* `worker.py` - calls the endpoint functions based on the inputted endpoint  
* Jobs/ - directory that contains output graphs and analysis  

### Docker/Implementation Files
* `main.py` - allows the user to start making API GET and POST methods  
* `DockerFile` - Containerizes the project  
* `docker-compose.yml` - allows to run multiple containers at once. Implements three databases used  
* `requirements.txt` - contains a list of python modules that are required to run API  
* `worker_requirements.txt` - contains a list of python modules required to run the worker.py file  


