#### Current Implementation:  



1. Clone repository.


2. cd into main directory.


3. If you do not have docker-compose installed, call "sudo apt install docker-compose".


4. From main directory, call "docker-compose up".


5. Open a separate terminal, and cd into main directory


6. If you do not have hotqueue, redis, matplotlib, numpy, or pandas installed, call 
"pip3 install -r worker_requirements.txt"


7. From main directory, call "python3 source/worker.py"


8. Open a separate terminal


9. Using curl or python, call an endpoint as demonstrated in README.md.


10. You will receive a confirmation message with your job ID and an endpoint to call to check your 
job's status and recieve your results.


11. When you are finished with the application, call "docker-compose down".




Note: If you have trouble successfully running docker-compose,
Try "sudo lsof -i :6379" to check whether port 6379 is in use.
If so, try "sudo kill -9 <process_id>". 

If redis-server is running, try "sudo service redis-server stop".
